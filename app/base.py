from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

class BaseClass:
    service_obj = Service(executable_path="app/resources/driver/chrome_linux/chromedriver")
    service_obj = Service()
    chrome_options = Options()
    # chrome_options.add_experimental_option('detach', True)
    chrome_options.add_argument('--no-sandbox') # Bypass OS security model
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-dev-shm-usage')
    driver = webdriver.Chrome(service=service_obj, options=chrome_options)
